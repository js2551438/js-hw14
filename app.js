const body = document.querySelector('body');
const changeThemeButton = document.querySelector('.change-theme-btn');


function initTheme () {
    if(localStorage.getItem('theme') === 'light') {
        body.classList.add('light-theme');
    } else {
        body.classList.remove('light-theme');
    }    
}

function changeTheme () {
    if(localStorage.getItem('theme') === 'light') {
        body.classList.remove('light-theme');
        localStorage.removeItem('theme');
    } else {
        localStorage.setItem('theme', 'light');
        body.classList.add('light-theme');
    }    
}

changeThemeButton.addEventListener('click', changeTheme)
initTheme();


// btn.addEventListener('click' , () =>{
//     if(localStorage.getItem('theme') != null  && localStorage.getItem('theme') != "#fff") {
//         localStorage.setItem('theme', '#fff');
//      } else {
//         localStorage.setItem('theme', '#171717');
//      }
//     firstTheme.style.background = localStorage.getItem('theme');
// }) 
